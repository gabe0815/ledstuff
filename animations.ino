#include "FastLED.h"

FASTLED_USING_NAMESPACE

// FastLED "100-lines-of-code" demo reel, showing just a few 
// of the kinds of animation patterns you can quickly and easily 
// compose using FastLED.  
//
// This example also shows one easy way to define multiple 
// animations patterns and have them automatically rotate.
//
// -Mark Kriegsman, December 2014

#if defined(FASTLED_VERSION) && (FASTLED_VERSION < 3001000)
#warning "Requires FastLED 3.1 or later; check github for latest code."
#endif


// ******************* LEDS ***********************************
#define DATA_PIN    6
//#define CLK_PIN   4
#define LED_TYPE    WS2812
#define COLOR_ORDER GRB
#define N_ROWS 32
#define N_COLUMNS 8

#define NUM_LEDS (N_ROWS * N_COLUMNS)

CRGB leds[NUM_LEDS];

#define SERPENTINE true

uint8_t BRIGHTNESS = 150;
uint8_t BRIGHTNESS_STEP = 10;

#define FRAMES_PER_SECOND  50


// ********************** BUTTONS ***************************
#include <Button.h>        //https://github.com/JChristensen/Button

#define ANIMATION_PIN 2    //Connect a tactile button switch (or something similar)
#define COLOR_PIN 3        //from Arduino pin 2 to ground.
#define PULLUP true        //To keep things simple, we use the Arduino's internal pullup resistor.
#define INVERT true        //Since the pullup resistor will keep the pin high unless the
                           //switch is closed, this is negative logic, i.e. a high state
                           //means the button is NOT pressed. (Assuming a normally open switch.)
#define DEBOUNCE_MS 20     //A debounce time of 20 milliseconds usually works well for tactile button switches.
#define LONG_PRESS 1000    //We define a "long press" to be 1000 milliseconds.
Button animationBtn(ANIMATION_PIN, PULLUP, INVERT, DEBOUNCE_MS);    //Declare the button
Button colorBtn(COLOR_PIN, PULLUP, INVERT, DEBOUNCE_MS);    //Declare the button


// ********************* VARIABLES **************************
long changeMillis = 50; 
int counter;
int counter2;
int n;
// ********************** ANIMATIONS AND PALETTES ***********

uint8_t gCurrentPatternNumber = 0; // Index number of which pattern is current
uint8_t gHue = 0; // rotating "base color" used by many of the patterns

uint8_t gCurrentPaletteNumber = 0;
CRGBPalette16 currentPalette;
TBlendType    currentBlending;


extern CRGBPalette16 myRedWhiteBluePalette;
extern const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM;

// *************** ANIMATIONS *******************************

void black(){
  fill_solid(leds, NUM_LEDS, CHSV(0,0,0));
}

void rainbow() {
  //make color changes slower
  changeMillis = 100; 
  //fill_solid(leds, NUM_LEDS, CHSV(gHue, 255, BRIGHTNESS));
  fill_solid(leds, NUM_LEDS, ColorFromPalette(currentPalette, gHue, BRIGHTNESS, currentBlending));
  
}

void rainbowStrip() {
  changeMillis = 200;
  if (counter >= N_COLUMNS){
    counter = 0;
  }
  //slowly dim the leds
  fadeToBlackBy( leds, NUM_LEDS, 50);
  //fill_solid(leds, NUM_LEDS, CHSV(0,0,0));
  for (int j=0; j<N_ROWS; j++){
    //leds[counter*N_ROWS + j] = ColorFromPalette(RainbowColors_p, gHue, BRIGHTNESS, LINEARBLEND);
   leds[counter*N_ROWS + j] = ColorFromPalette(currentPalette, gHue, BRIGHTNESS, currentBlending);
  }
}

void rainbowWithGlitter() 
{
  // built-in FastLED rainbow, plus some random sparkly glitter
  rainbow();
  addGlitter(150);
}

void addGlitter( fract8 chanceOfGlitter) 
{
  if( random8() < chanceOfGlitter) {
    leds[ random16(NUM_LEDS) ] += CRGB::White;
  }
}


void pulseRainbow(){
  changeMillis = 50; 
  uint8_t BeatsPerMinute = 100;
  uint8_t beat = beatsin8( BeatsPerMinute, BRIGHTNESS/4, BRIGHTNESS);
  //fill_solid(leds, NUM_LEDS, CHSV(gHue, 255, beat));
  fill_solid(leds, NUM_LEDS, ColorFromPalette(currentPalette, gHue, beat, currentBlending));
  
}

void rotateRainbow(){
  changeMillis = 20;
  uint8_t hue = gHue;
  for (int i=0; i<N_COLUMNS; i++){
    for (int j=0; j<N_ROWS; j++){
      //leds[i*N_ROWS + j] = CHSV(hue, 255, BRIGHTNESS);
      //leds[i*N_ROWS + j] = ColorFromPalette(RainbowColors_p, hue, BRIGHTNESS, LINEARBLEND);
      leds[i*N_ROWS + j] = ColorFromPalette(currentPalette, hue, BRIGHTNESS, currentBlending);
    }
    hue+=10;
  }
}

void rotateRainbowPulse(){
  changeMillis = 20;
  uint8_t hue = gHue;
  uint8_t BeatsPerMinute = 30;
  uint8_t beat = beatsin8( BeatsPerMinute, BRIGHTNESS/4, BRIGHTNESS);
 
  for (int i=0; i<N_COLUMNS; i++){
    for (int j=0; j<N_ROWS; j++){
      //leds[i*N_ROWS + j] = CHSV(hue, 255, BRIGHTNESS);
      //leds[i*N_ROWS + j] = ColorFromPalette(RainbowColors_p, hue, beat, LINEARBLEND);
      leds[i*N_ROWS + j] = ColorFromPalette(currentPalette, hue, beat, LINEARBLEND);
    }
    hue+=10;
  }
}

void strobo(){
  changeMillis = 10;
  if (gHue % 2 == 0){
    fill_solid(leds, NUM_LEDS, CRGB::White);
  } else {
    fill_solid(leds, NUM_LEDS, CRGB::Black);
  }
}

//void burstStrobe(){
//  Serial.println("burst");
//  changeMillis = 500;
//  if (counter % 10 == 0){
//    Serial.println("blink");
//    fill_solid(leds, NUM_LEDS, CRGB::White);
//    FastLED.show();
//    delay(50);
//  } else {
//    fill_solid(leds, NUM_LEDS, CRGB::Black); 
//  } 
//}

void plasma(){
  changeMillis = 5;

  if (counter >= N_ROWS){
    counter = 0;
  }
  
  fadeToBlackBy( leds, NUM_LEDS, 100);
  for (int i=0; i<N_COLUMNS; i++){
    if (i % 2 == 1){
      //reverse x
      leds[i*N_ROWS + (N_ROWS-counter-1)] = CRGB::White;
    } else {
      leds[i*N_ROWS + counter] = CRGB::White;
    }
  }
}

void plasma2(){
  changeMillis = 5;

  if (counter >= N_ROWS){
    counter = 0;
  }
  
  fadeToBlackBy( leds, NUM_LEDS, 100);
  for (int i=0; i<N_COLUMNS; i++){
    if (i % 2 == 1){
      //reverse x
      leds[i*N_ROWS + (N_ROWS-counter-1)] = ColorFromPalette(currentPalette, map(counter, 0, N_ROWS, 0,255), BRIGHTNESS, currentBlending);
    } else {
      leds[i*N_ROWS + counter] = ColorFromPalette(currentPalette, map(counter, 0, N_ROWS, 0,255), BRIGHTNESS, currentBlending);
    }
  }
}

//void icycle(){
//  changeMillis = 5;
//  
//  fadeToBlackBy( leds, NUM_LEDS, 100);
//  if (counter2 != 1){
//    if (random8() < 150){
//      counter2 = 1;
//      //define a column and start an icycle
//      n = random8(N_COLUMNS);
//    }
//  } else {
//    //draw the icycle
//    if (counter != 0){
//      //invert counter
//      if (n % 2 == 1){
//        //reverse x
//        leds[n*N_ROWS + (N_ROWS-counter-1)] = CRGB::White;
//      } else {
//        leds[n*N_ROWS + counter] = CRGB::White;
//      }
//    } else {
//        counter = N_ROWS;
//        counter2 = 0; 
//    }
//  }
//
//}

void spiral(){
   changeMillis = 5;

  if (counter >= NUM_LEDS){
    counter = 0;
  }

  int j = floor(counter / N_COLUMNS);
  int i = counter % N_COLUMNS; 
  fadeToBlackBy( leds, NUM_LEDS, 50);

  if (i % 2 == 1){
    //reverse x
    leds[i*N_ROWS + (N_ROWS-j-1)] = CRGB::White;
  } else {
    leds[i*N_ROWS + j] = CRGB::White;
  }
}

void confetti() 
{
  // random colored speckles that blink in and fade smoothly
  fadeToBlackBy( leds, NUM_LEDS, 10);
  int pos = random16(NUM_LEDS);
  leds[pos] += CHSV( gHue + random8(64), 200, 255);
}

void sinelon()
{
  // a colored dot sweeping back and forth, with fading trails
  fadeToBlackBy( leds, NUM_LEDS, 20);
  int pos = beatsin16(13,0,NUM_LEDS);
  leds[pos] += CHSV( gHue, 255, 192);
}

void bpm()
{
  // colored stripes pulsing at a defined Beats-Per-Minute (BPM)
  uint8_t BeatsPerMinute = 62;
  CRGBPalette16 palette = PartyColors_p;
  uint8_t beat = beatsin8( BeatsPerMinute, 64, 255);
  for( int i = 0; i < NUM_LEDS; i++) { //9948
    leds[i] = ColorFromPalette(palette, gHue+(i*2), beat-gHue+(i*10));
  }
}

void juggle() {
  // eight colored dots, weaving in and out of sync with each other
  fadeToBlackBy( leds, NUM_LEDS, 20);
  byte dothue = 0;
  for( int i = 0; i < 8; i++) {
    leds[beatsin16(i+7,0,NUM_LEDS)] |= CHSV(dothue, 200, 255);
    dothue += 32;
  }
}

void changePalette(){
  gCurrentPaletteNumber++;
  if (gCurrentPaletteNumber == 1){
    currentPalette = RainbowStripeColors_p; 
  } else if (gCurrentPaletteNumber == 2){
      currentPalette = CloudColors_p;
  } else if (gCurrentPaletteNumber == 3){
      currentPalette = PartyColors_p;
  } else if (gCurrentPaletteNumber == 4){
      SetupPurpleAndGreenPalette();
  } else if (gCurrentPaletteNumber == 5){
      currentPalette = HeatColors_p;
  } else if (gCurrentPaletteNumber == 6){
      setupAquaPalette();
  } else {
      gCurrentPaletteNumber = 0;
      currentPalette = RainbowColors_p;
  }
}

// ********************** PALETTES ********************************
//void SetupBlackAndWhiteStripedPalette()
//{
//    // 'black out' all 16 palette entries...
//    fill_solid( currentPalette, 16, CRGB::Black);
//    // and set every fourth one to white.
//    currentPalette[0] = CRGB::White;
//    currentPalette[4] = CRGB::White;
//    currentPalette[8] = CRGB::White;
//    currentPalette[12] = CRGB::White;
//    
//}

// This function sets up a palette of purple and green stripes.
void SetupPurpleAndGreenPalette()
{
    CRGB purple = CHSV( HUE_PURPLE, 255, 255);
    CRGB green  = CHSV( HUE_GREEN, 255, 255);
    CRGB black  = CRGB::Black;
    
    currentPalette = CRGBPalette16(
                                   green,  green,  black,  black,
                                   purple, purple, black,  black,
                                   green,  green,  black,  black,
                                   purple, purple, black,  black );
}

void setupAquaPalette(){
    currentPalette = CRGBPalette16( CRGB::Black, 
                     CRGB::Blue, 
                     CRGB::Aqua,  
                     CRGB::White);
}

// This example shows how to set up a static color palette
// which is stored in PROGMEM (flash), which is almost always more
// plentiful than RAM.  A static PROGMEM palette like this
// takes up 64 bytes of flash.
const TProgmemPalette16 myRedWhiteBluePalette_p PROGMEM =
{
    CRGB::Red,
    CRGB::Gray, // 'white' is too bright compared to red and blue
    CRGB::Blue,
    CRGB::Black,
    
    CRGB::Red,
    CRGB::Gray,
    CRGB::Blue,
    CRGB::Black,
    
    CRGB::Red,
    CRGB::Red,
    CRGB::Gray,
    CRGB::Gray,
    CRGB::Blue,
    CRGB::Blue,
    CRGB::Black,
    CRGB::Black
};


// List of patterns to cycle through.  Each is defined as a separate function below.
typedef void (*SimplePatternList[])();
//SimplePatternList gPatterns = { rainbow, rainbowWithGlitter, pulseRainbow, confetti, sinelon, juggle, bpm };
SimplePatternList gPatterns = {plasma, plasma2, rainbow, rainbowStrip, rainbowWithGlitter, rotateRainbow, rotateRainbowPulse, strobo, spiral, black };

// ******************** SETUP ********************************
void setup() {
  delay(1000); // 3 second delay for recovery
  // tell FastLED about the LED strip configuration
  FastLED.setMaxPowerInVoltsAndMilliamps(5,2000); 
  FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(leds, NUM_LEDS).setCorrection(TypicalLEDStrip);

  // set master brightness control
  FastLED.setBrightness(BRIGHTNESS);
  currentPalette = RainbowColors_p;
  currentBlending = LINEARBLEND;
  //Serial.begin(9600);
}


// ******************* MAIN LOOP *****************************
void loop()
{
  animationBtn.read();  //read the button
  if (animationBtn.pressedFor(LONG_PRESS)){
    fill_solid(leds, NUM_LEDS, CRGB::Black);
    if (BRIGHTNESS == 250) {
      BRIGHTNESS = 0;
    } else {
      BRIGHTNESS+=BRIGHTNESS_STEP;
    }
    for (int i = 0; i<map(BRIGHTNESS, 0,250,0,N_ROWS); i++){
      leds[i] = CHSV(HUE_PURPLE, 255, 200);  
    }
    FastLED.show();
    FastLED.setBrightness(BRIGHTNESS);
    
    delay(LONG_PRESS);
  } else if (animationBtn.wasReleased()) {
    nextPattern();
  }
  colorBtn.read();
  if (colorBtn.wasReleased()){
    changePalette();
    //Serial.println("change palette");
  }
  
    
  
  // Call the current pattern function once, updating the 'leds' array
  gPatterns[gCurrentPatternNumber]();
  // send the 'leds' array out to the actual LED strip
  FastLED.show();  
  // insert a delay to keep the framerate modest
  FastLED.delay(1000/FRAMES_PER_SECOND); 

  // do some periodic updates
  EVERY_N_MILLISECONDS( changeMillis ) { gHue++; } // slowly cycle the "base color" through the rainbow
  EVERY_N_MILLISECONDS( changeMillis ) { counter++; } // slowly cycle the "base color" through the rainbow
  //EVERY_N_SECONDS( 60 ) { nextPattern(); } // change patterns periodically
}

//******************************* HELPER FUNCTIONS ******************************

#define ARRAY_SIZE(A) (sizeof(A) / sizeof((A)[0]))

void nextPattern()
{
  // add one to the current pattern number, and wrap around at the end
  gCurrentPatternNumber = (gCurrentPatternNumber + 1) % ARRAY_SIZE( gPatterns);
}



